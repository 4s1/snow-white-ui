# Snow White - UI

_Currently the project does not work. I am working on it._

## ToDo

- [ ] remove moment
- [ ] remove axios
- [ ] enable Pipeline
- [ ] Remove local ESLint rules
- [ ] Docker publish
- [ ] 4s1 tsconfig
- [ ] ESM

## Sonstiges

Inhalt der `.env`

```text
REACT_APP_BACKEND_URL=http://localhost:3000
```
