import React from "react";
import Api from "../../../utils/api";
import Card from "../../common/Card";
import Select from "react-select";
import type { WeatherSettingsDto } from "../../../generated/dtos/weather-settings-dto";
import type { CommonLocationDto } from "../../../generated/dtos/common-location-dto";

interface IState {
  dto: WeatherSettingsDto;
  locations: CommonLocationDto[];
}

interface IProps {}

class Settings extends React.Component<IProps, IState> {
  private timer: any = null;

  constructor(props: IProps) {
    super(props);
    this.state = {
      dto: {
        apiKey: "",
        isActive: false,
        locationId: "",
      },
      locations: [],
    };

    this.onCheckboxIsActiveChange = this.onCheckboxIsActiveChange.bind(this);
    this.onTextApiKeyChange = this.onTextApiKeyChange.bind(this);
    this.onSelectLocationChange = this.onSelectLocationChange.bind(this);
  }

  public async componentDidMount(): Promise<void> {
    const dto: WeatherSettingsDto = await Api.get<WeatherSettingsDto>(
      "/v1/smartmirror/admin/weather/settings",
    );
    const locations: CommonLocationDto[] = await Api.get<CommonLocationDto[]>(
      "/v1/smartmirror/admin/common/locations",
    );

    this.setState({
      dto,
      locations: locations.sort(
        (a: CommonLocationDto, b: CommonLocationDto): number => a.sortNo - b.sortNo,
      ),
    });
  }

  public render(): JSX.Element {
    return (
      <Card title="Einstellungen">
        <form>
          <div className="form-group row">
            <label className="col-sm-4 col-form-label">Widget aktiv</label>
            <div className="col-sm-8">
              <div className="form-check">
                <input
                  className="form-check-input"
                  type="checkbox"
                  checked={this.state.dto.isActive}
                  onChange={this.onCheckboxIsActiveChange}
                />
              </div>
            </div>
          </div>
          <div className="form-group row">
            <label className="col-sm-4 col-form-label">API-Key</label>
            <div className="col-sm-8">
              <input
                type="text"
                className="form-control"
                disabled={!this.state.dto.isActive}
                value={this.state.dto.apiKey}
                onChange={this.onTextApiKeyChange}
              />
              <div className="text-right">
                <a
                  href="https://openweathermap.org/appid/"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  API-Key beantragen
                </a>
              </div>
            </div>
          </div>
          <div className="form-group row">
            <label className="col-sm-4 col-form-label">Ort</label>
            <div className="col-sm-8">
              <Select
                isDisabled={!this.state.dto.isActive}
                options={this.state.locations}
                onChange={this.onSelectLocationChange}
                value={this.state.locations.filter(
                  (location: CommonLocationDto): boolean =>
                    location.id === this.state.dto.locationId,
                )}
                getOptionLabel={(option: CommonLocationDto): string => option.name}
                getOptionValue={(option: CommonLocationDto): string => option.id}
                placeholder={"Bitte auswählen..."}
              />
            </div>
          </div>
        </form>
      </Card>
    );
  }

  private onSelectLocationChange(value: any): void {
    this.setState(
      {
        dto: {
          ...this.state.dto,
          locationId: value.id,
        },
      },
      this.saveValues,
    );
  }

  private onCheckboxIsActiveChange(e: React.ChangeEvent<HTMLInputElement>): void {
    this.setState(
      {
        dto: {
          ...this.state.dto,
          isActive: e.currentTarget.checked,
        },
      },
      this.saveValues,
    );
  }

  private onTextApiKeyChange(e: React.ChangeEvent<HTMLInputElement>): void {
    this.setState(
      {
        dto: {
          ...this.state.dto,
          apiKey: e.currentTarget.value,
        },
      },
      this.saveValues,
    );
  }

  private saveValues(): void {
    clearTimeout(this.timer);
    this.timer = setTimeout(async () => {
      await Api.put<WeatherSettingsDto>("/v1/smartmirror/admin/weather/settings", this.state.dto);
    }, 333);
  }
}

export default Settings;
