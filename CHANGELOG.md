# Changelog

## 1.1.2 (2022-04-03)

## 1.1.1 (2021-11-21)

## 1.1.0 (2021-11-13)

## 1.0.5 (2021-11-07)

## 1.0.4 (2021-11-07)

- fix: typo in title

## 1.0.3 (2021-11-07)

## 1.0.2 (2021-11-07)

### 1.0.1 (2021-11-07)

- feat: use ProcessEnv for env variables
- fix: degree sign
- fix: url in package.json
